#!/bin/bash
if [ -z "${EX_DIR}" ]; then
	EX_DIR=$(dirname "${BASH_SOURCE[0]}")
fi

export LD_PRELOAD=""
export LD_LIBRARY_PATH=""
sudo rm /usr/lib/libfunction.so
# end of comment

# compile example
cd "${EX_DIR}"; make

# libfunction with not found
ldd "${EX_DIR}/main"

# add to default library paths
sudo cp "${EX_DIR}/libfunction.so" /usr/lib/

# libfunction at /usr/lib/libfunction.so
ldd "${EX_DIR}/main"

# create another path
mkdir "${EX_DIR}/tmp_dir_1"; cp "${EX_DIR}/libfunction.so" "${EX_DIR}/tmp_dir_1" 

# check RUNPATH
objdump -x "${EX_DIR}/main" | grep RUNPATH

# set DT_RUNPATH
patchelf --set-rpath "${EX_DIR}/tmp_dir_1" "${EX_DIR}/main"

# check RUNPATH
objdump -x main | grep RUNPAT

# libfunction at new absolute path 
ldd "${EX_DIR}/main"

# create another path
mkdir "${EX_DIR}/tmp_dir_1/tmp_dir_2"; cp "${EX_DIR}/libfunction.so" "${EX_DIR}/tmp_dir_1/tmp_dir_2" 

# export LD_LIBRARY_PATH
export LD_LIBRARY_PATH="${EX_DIR}/tmp_dir_1/tmp_dir_2"

# libfunction at new absolute path 
ldd "${EX_DIR}/main"

# create another path
mkdir "${EX_DIR}/tmp_dir_1/tmp_dir_2/tmp_dir_3"; cp "${EX_DIR}/libfunction.so" "${EX_DIR}/tmp_dir_1/tmp_dir_2/tmp_dir_3" 

# export LD_LIBRARY_PATH
export LD_PRELOAD="${EX_DIR}/tmp_dir_1/tmp_dir_2/tmp_dir_3/libfunction.so"

# libfunction at new absolute path 
ldd "${EX_DIR}/main"
