#!/bin/bash
if [ -z "${EX_DIR}" ]; then
	EX_DIR=$(dirname "${BASH_SOURCE[0]}")
fi
# end of comment

# compile example
cd "${EX_DIR}"; make

# symbol table
nm "${EX_DIR}/main"

# reducing external symbols
strip "${EX_DIR}/main"

# table reduced
nm "${EX_DIR}/main"

# calling main
"${EX_DIR}/main"
