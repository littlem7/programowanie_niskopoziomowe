#!/bin/bash
if [ -z "${EX_DIR}" ]; then
	EX_DIR=$(dirname "${BASH_SOURCE[0]}")
fi
# end of comment


# compile example
cd "${EX_DIR}"; make

# change source of library
vim "${EX_DIR}/function.c"

# remove previous library
rm "${EX_DIR}/libfunction.so"

# rebuild library
cd "${EX_DIR}"; make libfunction.so

# execute new main
export LD_LIBRARY_PATH="${EX_DIR}"; ${EX_DIR}/main
