#!/bin/bash
if [ -z "${EX_DIR}" ]; then
	EX_DIR=$(dirname "${BASH_SOURCE[0]}")
fi
# end of comment

# compile example
cd "${EX_DIR}"; make

# there are few sections main binary file
readelf --sections "${EX_DIR}/main.o"

# table of symbols 
nm "${EX_DIR}/main"
