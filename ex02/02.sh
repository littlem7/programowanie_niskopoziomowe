#!/bin/bash
if [ -z "${EX_DIR}" ]; then
	EX_DIR=$(dirname "${BASH_SOURCE[0]}")
fi
# end of comment

# view of main.c
vim "${EX_DIR}/main.c"

# view of function.c
vim "${EX_DIR}/function.c"

# compile example
cd "${EX_DIR}"; make

# disasembly output of main.o (with Intel addembler format)
# section .text (main) call to self
# resolving references should be applied
objdump -D -M intel "${EX_DIR}/main.o" | less

# disasembly output of main
# section .text

objdump -D -M intel "${EX_DIR}/main" | less