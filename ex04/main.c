#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>

int main(int argc, char **argv) {
    void *handle;
    double (*cosine)(double);
    char *error;

    // dlopen opens a library and prepares it for use
    // RTLD_LAZY - resolve undefined symbols as code from the dynamic library is executed
    handle = dlopen ("libm.so.6", RTLD_LAZY); 
    
    // dlerror returns a string describing the error from the last call 
     if (!handle) {
        fputs (dlerror(), stderr);
        exit(1);
    }

    // dlsym looks up the value of a symbol in a given (opened) library
    cosine = dlsym(handle, "cos");
    if ((error = dlerror()) != NULL)  {
        fputs(error, stderr);
        exit(1);
    }

    printf ("%f\n", (*cosine)(2.0));
    
    // closes a DL library
    dlclose(handle);
}