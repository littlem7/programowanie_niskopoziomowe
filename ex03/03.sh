#!/bin/bash
if [ -z "${EX_DIR}" ]; then
	EX_DIR=$(dirname "${BASH_SOURCE[0]}")
fi
# end of comment

# compile example
cd "${EX_DIR}"; make

# list files at archive
ar -t libfunction.a

# rm function.o file
rm "${EX_DIR}/function.o"

# ls directory
ls "${EX_DIR}"

# extract objects
ar -xv libfunction.a  function.o

# ls directory
ls "${EX_DIR}"

# change function.c
vim "${EX_DIR}/function.c"

# recompile function.o
rm "${EX_DIR}/function.o"; cd "${EX_DIR}"; make function.o

# recompile arch
cd "${EX_DIR}"; make libfunction.a

# recompile main
cd "${EX_DIR}"; make main

# execute main
cd "${EX_DIR}"; ./main