#!/bin/bash
if [ -z "${EX_DIR}" ]; then
	EX_DIR=$(dirname "${BASH_SOURCE[0]}")
fi
# end of comment

# compile example
cd "${EX_DIR}"; make

# header of the file
objdump -f "${EX_DIR}/main.o"

