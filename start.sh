#!/bin/bash

exercise_num="${1}"
exercise_script="./ex${exercise_num}/${exercise_num}.sh"


function echo_blue(){
	echo -e "\033[0;34m${1}\033[0m"
}

function echo_green(){
	echo -e "\033[1;32m${1}\033[0m"
}

function echo_red(){
	echo -e "\033[0;31m${1}\033[0m"
}

function echo_error(){
	echo_red "[ERROR] ${1}"
}

function echo_line(){
	echo_green "${1}"
}

function wait_for_next_step(){
	while read -s -n1 -p ">>>" input; do
		case "${input}" in
			"") echo ; break;;
			*) echo "press <enter> to go";;
		esac
	done
}

# protection
if [ -z "${exercise_num}" ]; then
	echo_error "start.sh <exercise_num>"
	exit 1
elif [ ! -x "${exercise_script}" ]; then
	echo_error "${exercise_script} should be executable"
	exit 1
fi

# main script
export EX_DIR="$(pwd)/ex${exercise_num}"
start_script=0

IFS=$'\n' 
for line in $(cat "${exercise_script}"); do
	if [ -n "${line}" ] && [[ ! "${line}" =~ ^#.* ]] && [ "${start_script}" -eq 1 ]; then # ignoring empty lines & comments with shebang
    	echo_line "\n${line}"
    	wait_for_next_step
    	eval "${line}" # execute line
   	fi

   	if [ "${line}" == "# end of comment" ]; then
		start_script=1
	fi
done